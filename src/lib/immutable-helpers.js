export function insertItem(array, item) {
  const newArray = array.slice();
  newArray.splice(0, 0, item);
  return newArray;
}

export function removeItem(array, item) {
  const index = array.indexOf(item);
  const newArray = array.slice();
  newArray.splice(index, 1);
  return newArray;
}

export function replaceItemAtIndex(array, item, index) {
  const newArray = array.slice();
  newArray.splice(index, 1, item);
  return newArray;
}
