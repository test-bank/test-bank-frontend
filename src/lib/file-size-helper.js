export function getReadableSize(bytes) {
  if (bytes >= 1000000000) {
    return `${round(bytes/1000000000, 1)} gb`;
  }
  if (bytes >= 1000000) {
    return `${round(bytes/1000000, 1)} mb`;
  }
  if (bytes >= 1000) {
    return `${round(bytes/1000, 1)} kb`;
  }
  return `${bytes} bytes`;
}

function round(value, precision) {
  const multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}
