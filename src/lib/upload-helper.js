export function checkDirectoryForFile(directory, file, category) {
  if (category && directory[category]) {
    return checkFolder(file, directory[category]);
  }
  return Object.keys(directory).some(folder => checkFolder(file, directory[folder]));
}

function checkFolder(file, folder) {
  return folder.some(f => f.name === file.name);
}
