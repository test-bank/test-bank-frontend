import React from 'react';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import autobind from 'autobind-decorator';

import User from '../../modules/user';
import Admin from '../../modules/admin';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import { LinearProgress } from 'material-ui/Progress';
import Menu, { MenuItem } from 'material-ui/Menu';
import { ListItem, ListItemText } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import AccountCircle from 'material-ui-icons/AccountCircle';
import AccountBox from 'material-ui-icons/AccountBox';
import Divider from 'material-ui/Divider';

import '../../app/styles/components/testbank-bar.sass';

class TestbankAppBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: undefined,
      open: false
    };
  }

  @autobind
  handleClick(event) {
    this.setState({ open: true, anchorEl: event.currentTarget });
  }

  @autobind
  handleRequestClose() {
    this.setState({ open: false });
  }

  @autobind
  logout() {
    this.props.dispatch(User.actions.logout());
  }

  @autobind
  toggleAdminPage() {
    this.setState({ open: false });
    this.props.dispatch(Admin.actions.toggleAdminPage());
  }

  renderMenu() {
    const { username, role } = this.props.currentUser;
    return (
      <Menu id="simple-menu" anchorEl={this.state.anchorEl}
        open={this.state.open} onRequestClose={this.handleRequestClose}
      >
        <ListItem className="app-bar-menu-user">
          <Avatar>
            <AccountCircle />
          </Avatar>
          <ListItemText primary={username} secondary={role} />
        </ListItem>
        {role === 'admin' ? <Divider light /> : null}
        {role === 'admin' ? <MenuItem onClick={this.toggleAdminPage}>Admin Page</MenuItem> : null}
        <Divider light />
        <MenuItem onClick={this.logout}>Logout</MenuItem>
      </Menu>
    );
  }

  render() {
    const progress = this.props.isLoading ? <LinearProgress size={50} mode="indeterminate" className="appbar-progress"/> : null;
    return (
      <ReactCSSTransitionGroup
        transitionName="app-bar"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <AppBar position="static" className="app-bar">
          <Toolbar>
            <Typography type="title" color="inherit">
              Testbank
            </Typography>
            <IconButton className="account-box-button"
              onClick={this.handleClick}
            >
              <AccountBox className="account-box" />
            </IconButton>
          </Toolbar>
          {progress}
        </AppBar>
        {this.renderMenu()}
      </ReactCSSTransitionGroup>
    );
  }
}


const mapStateToProps = state => {
  return {
    currentUser: state.user.currentUser,
    isLoading: state.testbank.isFetching
  };
};

export default connect(
  mapStateToProps
)(TestbankAppBar);
