import React from 'react';
import { connect } from 'react-redux';

import AppBar from './TestbankBar';
import CourseList from './CourseList';

import '../../app/styles/components/testbank.sass';

class Testbank extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const className = !this.props.isAdminPageOpen ? 'testbank' : 'testbank-closed';
    return (
      <div className={className}>
        <AppBar />
        <CourseList />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAdminPageOpen: state.admin.isAdminPageOpen
  };
};

export default connect(
  mapStateToProps
)(Testbank);
