import React from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import { connect } from 'react-redux';
import fetch from 'isomorphic-fetch';
import config from 'config';

// components
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Dropzone from 'react-dropzone';
import Divider from 'material-ui/Divider';
import List, { ListItem, ListItemText, ListItemIcon } from 'material-ui/List';
import { CircularProgress } from 'material-ui/Progress';

// icons
import ExpandMore from 'material-ui-icons/ExpandMore';
import UploadIcon from 'material-ui-icons/FileUpload';
import DeleteIcon from 'material-ui-icons/Delete';
import FolderIcon from 'material-ui-icons/Folder';
import FileIcon from 'material-ui-icons/InsertDriveFile';
import BackIcon from 'material-ui-icons/ArrowBack';
import ForwardIcon from 'material-ui-icons/ArrowForward';
import EditIcon from 'material-ui-icons/ModeEdit';
import EndEditIcon from 'material-ui-icons/Done';

// lib
import { getReadableSize } from '../../lib/file-size-helper';
import { checkDirectoryForFile } from '../../lib/upload-helper';

//modules
import Upload from '../../modules/upload';
import Testbank from '../../modules/testbank';

// styles
import '../../app/styles/components/course-card.sass';

class CourseCard extends React.Component {
  constructor(props) {
    super(props);
    const currentPath = props.title + '/' + Object.keys(this.props.directory)[0];
    this.state = {
      isHoveringFooter: false,
      uploadOpen: false,
      dirOpen: false,
      files: [],
      fileCategories: {},
      currentPath,
      offset: false,
      editMode: false
    };
  }

  @autobind
  handleUpload(e) {
    e.stopPropagation();
    this.setState({ uploadOpen: !this.state.uploadOpen, dirOpen: false });
  }

  @autobind
  completeUpload() {
    this.setState({ uploadOpen: !this.state.uploadOpen });
    this.state.files.forEach(file => {
      if (checkDirectoryForFile(this.props.directory, file, this.state.fileCategories[file.name])) {
        this.props.dispatch(Upload.actions.uploadFailed(this.props.title, file));
      } else {
        this.props.dispatch(Upload.actions.uploadFile(this.props.title, file));
      }
    });
    this.setState({ files: [] });
  }

  handleDelete(file, event) {
    event.stopPropagation();
    const files = this.state.files;
    const index = files.indexOf(file);
    files.splice(index, 1);
    this.setState({ files });
  }

  getCategories(files) {
    if (files.length === 0) return;
    const body = files.map(file => ({ name: file.name, size: file.size }));
    const params = {
      body: JSON.stringify({ files: body }),
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      credentials: 'include'
    };
    fetch(`${config.api.baseUrl}/api/testbank/upload/category`, params)
    .then(response => response.json())
    .then(json => {
      const fileCategories = Object.assign({}, this.state.fileCategories, json.categories);
      this.setState({ fileCategories });
    });
  }

  @autobind
  footerClicked() {
    const { uploadOpen, dirOpen } = this.state;
    if (uploadOpen || dirOpen) {
      this.setState({ uploadOpen: false, dirOpen: false, editMode: false });
    } else {
      this.setState({ dirOpen: true });
    }
  }

  @autobind
  onDrop(fs) {
    const current = this.state.files;
    const toAdd = fs.filter(file => !current.some(f => file.name === f.name));
    this.getCategories(toAdd);
    const files = current.concat(toAdd);
    this.setState({ files });
  }

  renderDropZoneText(text) {
    return <Typography className="cc-dropzone-text" type="title">{text}</Typography>;
  }

  renderDropZoneFileList() {
    const files = this.state.files.map((file, index) => {
      const category = this.state.fileCategories[file.name]
      ? <Chip label={this.state.fileCategories[file.name]} />
      : <CircularProgress size={24} />;

      const title = <Typography className="cc-dropzone-filelist-title">{file.name}</Typography>;
      return (
        <ListItem dense key={index} divider={index !== this.state.files.length - 1}>
          <ListItemText primary={title} secondary={getReadableSize(file.size)} />
          {category}
          <IconButton aria-label="Delete"
            onClick={(event) => this.handleDelete(file, event)}
          >
            <DeleteIcon />
          </IconButton>
        </ListItem>
      );
    });
    return <List className="cc-dropzone-filelist">{files}</List>;
  }

  renderDropZone() {
    if (this.state.uploadOpen) {
      return (
        <Dropzone onDrop={this.onDrop} className="cc-dropzone" activeClassName="cc-dropzone-active">
          {({ isDragActive }) => {
            if (isDragActive) {
              return this.renderDropZoneText('Drop it fuck face');
            }
            return this.state.files.length
            ? this.renderDropZoneFileList()
            : this.renderDropZoneText('Click or Drop Files Here');
          }}
        </Dropzone>
      );
    }
    return null;
  }

  openFile(event, href) {
    event.stopPropagation();
    event.preventDefault();
    const redirectWindow = window.open(href, '_blank');
    redirectWindow.location;
  }

  deleteFile(event, file) {
    event.stopPropagation();
    event.preventDefault();
    this.props.dispatch(Testbank.actions.deleteFile(file, this.props.title));
  }

  handleDirClick(event, key) {
    event.stopPropagation();
    event.preventDefault();
    this.setState({ currentPath: `${this.props.title}/${key}`, offset: true });
  }

  renderDirectoryItems() {
    return Object.keys(this.props.directory).map(key => {
      return (
          <ListItem dense button onClick={(event) => this.handleDirClick(event, key)} key={key} >
            <ListItemIcon>
              <FolderIcon />
            </ListItemIcon>
            <ListItemText primary={key} />
            <Chip label={`${this.props.directory[key].length} files`} />
          </ListItem>
      );
    });
  }

  renderFileItems() {
    const currentDir = this.state.currentPath.split('/')[1];
    if (!this.props.directory[currentDir]) return null;
    return this.props.directory[currentDir].map(file => {
      const chip = !this.state.editMode
      ? <Chip label={getReadableSize(file.size)} />
      : this.props.deleting.indexOf(file.name) === -1 ?
        (
          <IconButton onClick={(event) => this.deleteFile(event, file.name)} className="cc-filelist-icon-delete">
            <DeleteIcon />
          </IconButton>
        ) : <CircularProgress size={24} />;

      return (
        <ListItem button dense
          onClick={(event) => this.openFile(event, file.href)} key={file.name} >
          <ListItemIcon className="cc-filelist-icon">
            <FileIcon />
          </ListItemIcon>
          <Typography className="cc-filelist-itemtext">{file.name}</Typography>
          {chip}
        </ListItem>
      );
    });
  }

  renderFileList() {
    const currentDir = this.state.currentPath.split('/')[1];
    const className = this.state.offset && this.props.directory[currentDir] ? 'cc-filelist-transformed' : 'cc-filelist';
    if (this.state.dirOpen) {
      return (
        <div className="cc-filelist-container">
          <List className={className}>
            {this.renderDirectoryItems()}
          </List>
          <List className={className}>
            {this.renderFileItems()}
          </List>
        </div>
      );
    }
    return null;
  }

  @autobind
  goBack(event) {
    event.preventDefault();
    event.stopPropagation();
    this.setState({ offset: false, editMode: false });
  }

  @autobind
  goForward(event) {
    event.preventDefault();
    event.stopPropagation();
    this.setState({ offset: true });
  }

  @autobind
  beginEditing(event) {
    event.preventDefault();
    event.stopPropagation();
    this.setState({ editMode: true });
  }

  @autobind
  endEditing(event) {
    event.preventDefault();
    event.stopPropagation();
    this.setState({ editMode: false });
  }

  renderNavBar() {
    if (this.state.dirOpen) {
      const title = this.state.offset ? this.state.currentPath.split('/')[this.state.currentPath.split('/').length - 1] : this.props.title;
      const backButton = this.state.offset
      ? (
        <IconButton onClick={this.goBack} className="cc-navbar-icon-back">
          <BackIcon />
        </IconButton>
      ) : <div className="cc-navbar-icon-back" />;

      let rightButtonFunc = this.goForward;
      let rightButtonIcon = <ForwardIcon />;

      if (this.state.editMode) {
        rightButtonIcon = <EndEditIcon />;
        rightButtonFunc = this.endEditing;
      } else if (this.state.offset) {
        rightButtonIcon = this.props.currentUser.role !== 'user' ? <EditIcon /> : null;
        rightButtonFunc = this.beginEditing;
      }

      return (
        <div className="cc-navbar">
          {backButton}
          <Typography className="cc-navbar-title">
            {title}
          </Typography>
          <IconButton onClick={rightButtonFunc} className="cc-navbar-icon-forward"
            disabled={this.props.currentUser.role === 'user' && this.state.offset}
           >
            {rightButtonIcon}
          </IconButton>
        </div>
      );
    }
  }

  renderHeaderContainer() {
    return (
      <div className="cc-header-container">
        <Typography type="title" className="cc-title" component="h2">
          {this.props.title}
        </Typography>
        <Typography type="title" className="cc-size-label" component="h2">
          {getReadableSize(this.props.size)}
        </Typography>
      </div>
    );
  }

  renderTitleContainer() {
    return (
      <div className="cc-title-container">
        <span className="cc-title-bar" />
        {this.renderHeaderContainer()}
        <div className="cc-stat-container">
          <Chip label={`${this.props.count} files`} className="cc-stat"/>
        </div>
      </div>
    );
  }

  renderFooterContainer() {
    const hoverIconClass = this.state.isHoveringFooter ? ' cc-expand-more-icon-hover' : '';
    const iconClass = (this.state.uploadOpen || this.state.dirOpen ? 'cc-expand-more-icon-open' : 'cc-expand-more-icon') + hoverIconClass;
    const uploadButton = this.state.uploadOpen
    ? (
      <Button raised className="cc-doupload-button"
        disabled={this.state.files.length === 0}
        onClick={this.completeUpload}
      >
        Upload
      </Button>
    ) : (
      <IconButton className="cc-upload-button"
        onClick={this.handleUpload}
      >
        <UploadIcon />
      </IconButton>
    );
    return (
      <div className="cc-footer-container" onClick={this.footerClicked}>
        {this.props.currentUser.role === 'user' ? <div className="cc-upload-button" /> : uploadButton}
        <ExpandMore className={iconClass}/>
      </div>
    );
  }

  render() {
    const className = this.state.uploadOpen
    ? 'cc-course-card-upload'
    : this.state.dirOpen
      ? 'cc-course-card-directory'
      : 'cc-course-card';
    const divider = this.state.uploadOpen || this.state.dirOpen ? <Divider /> : null;
    return (
      <Card className={className} raised
        onMouseOver={() => this.setState({ isHoveringFooter: true })}
        onMouseLeave={() => this.setState({ isHoveringFooter: false })}
        onClick={this.footerClicked}
      >
        <CardContent className="cc-course-card-content">
          {this.renderTitleContainer()}
          {this.renderNavBar()}
          {divider}
          {this.renderDropZone()}
          {this.renderFileList()}
          {divider}
          {this.renderFooterContainer()}
        </CardContent>
      </Card>
    );
  }
}

CourseCard.PropTypes = {
  title: PropTypes.string.isRequired,
  directory: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    currentUser: state.user.currentUser,
    deleting: state.testbank.deleting
  };
};

export default connect(
  mapStateToProps
)(CourseCard);
