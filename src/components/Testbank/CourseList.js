import React from 'react';
import { connect } from 'react-redux';

import TestbankActions from '../../modules/testbank/actions';
import Grid from 'material-ui/Grid';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import CourseCard from './CourseCard';

import '../../app/styles/components/course-list.sass';

class CourseList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false };
  }

  componentDidMount() {
    this.props.dispatch(TestbankActions.fetchTestbank())
      .then(() => this.setState({ loaded: true }));
  }

  renderCourses() {
    return this.props.testbank.map(course => {
      const title = course.name;
      const directory = course.directory;
      const size = course.size;
      const count = course.count;
      return <CourseCard title={title} directory={directory} size={size} count={count} key={title}/>;
    });
  }

  render() {
    return this.state.loaded
      ? (
        <ReactCSSTransitionGroup
          transitionName="course-list"
          transitionAppear={true}
          transitionAppearTimeout={500}
          transitionEnter={false}
          transitionLeave={false}
        >
          <Grid container justify="center" className="course-grid" align="flex-start">
            {this.renderCourses()}
          </Grid>
        </ReactCSSTransitionGroup>
      ) : null;
  }
}

const mapStateToProps = state => {
  return {
    testbank: state.testbank.courses
  };
};

const connectedCourseList = connect(
  mapStateToProps
)(CourseList);

export default connectedCourseList;
