import React from 'react';
import flux from '../modules/flux';
import { Provider } from 'react-redux';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import * as Colors from 'material-ui/colors';

import App from './app';

const theme = createMuiTheme({
  palette: {
    primary: Colors.indigo500,
    secondary: Colors.cyanA400
  }
});

class Root extends React.Component {
  render() {
    return (
      <Provider store={flux}>
        <MuiThemeProvider theme={theme}>
          <App />
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default Root;
