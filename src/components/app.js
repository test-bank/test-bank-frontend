import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';

import Admin from '../modules/admin';
import User from '../modules/user';

import Dialog from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';

import Login from './Login';
import Testbank from './Testbank';
import AdminView from './Admin';
import Upload from './Upload';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      didFetch: false
    };
  }

  componentDidMount() {
    this.props.dispatch(User.actions.fetchCurrentUser())
      .then(() => {
        this.setState({ didFetch: true });
      });
  }

  @autobind
  handleRequestClose() {
    this.props.dispatch(Admin.actions.toggleAdminPage());
  }

  renderAdminDialog() {
    const slide = <Slide direction="left" enterTransitionDuration={500} leaveTransitionDuration={500} />;
    return this.props.currentUser && this.props.currentUser.role === 'admin'
      ? (
        <Dialog
          fullScreen
          open={this.props.isAdminPageOpen}
          onRequestClose={this.handleRequestClose}
          transition={slide}
        >
          <AdminView />
        </Dialog>
      ) : null;
  }

  render() {
    const currentView = this.state.didFetch ? this.props.currentUser ? <Testbank /> : <Login /> : null;
    return (
      <div>
        {currentView}
        {this.renderAdminDialog()}
        <Upload />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.user.currentUser,
    isAdminPageOpen: state.admin.isAdminPageOpen
  };
};

const ConnectedApp = connect(
  mapStateToProps
)(App);

export default ConnectedApp;
