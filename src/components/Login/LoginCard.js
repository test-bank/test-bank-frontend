import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';

import User from '../../modules/user';

import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Input from 'material-ui/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import { LinearProgress } from 'material-ui/Progress';

class LoginCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  @autobind
  login() {
    const { username, password } = this.state;
    this.props.dispatch(User.actions.login(username, password));
  }

  @autobind
  handleKeyDown(event) {
    if (event.keyCode === 13) this.login();
  }

  @autobind
  registerClicked() {
    this.props.onRegisterClick();
    this.props.dispatch(User.actions.clearErrors());
  }

  renderErrorMessage() {
    const message = this.props.loginError;
    return message
      ? (
        <Typography className="error">
          {message}
        </Typography>
      )
      : null;
  }

  renderProgress() {
    return this.props.isLoggingIn
      ? <LinearProgress className="progress" size={50} mode="indeterminate" />
      : null;
  }

  render() {
    const buttonDisabled = this.state.username === '' || this.state.password === '';
    const className = this.props.isInMiddle ? 'card-login-middle' : 'card-login';
    return (
      <Card className={className}>
        <CardContent className="card-content">
          <div className="title-container">
            <span className="title-bar"/>
            <Typography type="title" className="title" component="h2">
              LOGIN
            </Typography>
          </div>
          <FormControl className="form-control">
            <InputLabel htmlFor="username">Username</InputLabel>
            <Input id="username" onChange={event => this.setState({ username: event.target.value })} />
          </FormControl>
          <FormControl className="form-control">
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input id="password" type="password"
              onChange={event => this.setState({ password: event.target.value })}
              onKeyDown={event => this.handleKeyDown(event)}
            />
          </FormControl>
          <Button raised color="primary" className="button"
            onClick={this.login}
            disabled={buttonDisabled}
          >
            Go
          </Button>
          <Button color="primary" disableRipple
            className="no-account-button"
            onClick={this.registerClicked}
          >
            {`Don't have an account?`}
          </Button>
          {this.renderErrorMessage()}
          {this.renderProgress()}
        </CardContent>
      </Card>
    );
  }
}

LoginCard.PropTypes = {
  onRegisterClick: PropTypes.func.isRequired,
  isInMiddle: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  return {
    isLoggingIn: state.user.isLoggingIn,
    loginError: state.user.loginError
  };
};

const ConnectedLogin = connect(
  mapStateToProps
)(LoginCard);

export default ConnectedLogin;
