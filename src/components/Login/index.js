import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import autobind from 'autobind-decorator';

import LoginCard from './LoginCard';
import RegisterCard from './RegisterCard';

import '../../app/styles/components/login.sass';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onLoginPage: true
    };
  }

  @autobind
  renderLoginCard() {
    return <LoginCard isInMiddle={!this.state.onLoginPage} onRegisterClick={() => this.setState({ onLoginPage: false })} />;
  }

  @autobind
  renderRegisterCard() {
    const classes = this.props.classes;
    return !this.state.onLoginPage
      ? <RegisterCard closeButtonClicked={() => this.setState({ onLoginPage: true })} classes={classes} />
      : null;
  }

  render() {
    const classNameStack = this.state.onLoginPage ? 'card-stack' : 'card-stack-back';
    const classNameBack = this.state.onLoginPage ? 'card-top': 'card-top-back';
    return (
      <div className="card-container">
        <div className={classNameStack}>
          <div className={classNameBack} />
          {this.renderLoginCard()}
        </div>
        <ReactCSSTransitionGroup transitionName="slide-in"
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}
        >
          {this.renderRegisterCard()}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default Login;
