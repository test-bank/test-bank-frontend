import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import User from '../../modules/user';
import autobind from 'autobind-decorator';

import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Input from 'material-ui/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import FormControl from 'material-ui/Form/FormControl';
import Button from 'material-ui/Button';
import Close from 'material-ui-icons/Close';
import IconButton from 'material-ui/IconButton';
import { LinearProgress } from 'material-ui/Progress';

class RegisterCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      accessKey: ''
    };
  }

  renderErrorMessage() {
    const message = this.props.registrationError;
    return message
      ? (
        <Typography className="error-signup" key="error-signup">
          {message}
        </Typography>
      ) : null;
  }

  @autobind
  register() {
    const { username, password, accessKey } = this.state;
    this.props.dispatch(User.actions.register(username, password, accessKey));
  }

  @autobind
  handleKeyDown(event) {
    if (event.keyCode === 13) this.register();
  }

  @autobind
  closeClicked() {
    this.props.closeButtonClicked();
    this.props.dispatch(User.actions.clearErrors());
  }

  renderProgress() {
    return this.props.isRegistering
      ? <LinearProgress key="progress" className="progress" size={50} mode="indeterminate" />
      : null;
  }

  render() {
    const buttonDisabled = this.state.username === '' || this.state.password === '' || this.state.accessKey === '';
    return (
      <Card className="card-register" key="card">
        <CardContent className="card-content" key="card-content">
          <div className="title-container" key="title-container">
            <span className="title-bar" key="title-bar"/>
            <Typography type="title" className="title" component="h2" key="title">
              REGISTER
            </Typography>
            <IconButton className="icon-button" aria-label="Close" key="icon-button"
              onClick={this.closeClicked}
            >
              <Close className="close-icon" key="close-icon"/>
            </IconButton>
          </div>
          <FormControl className="form-control" key="form-control-1">
            <InputLabel htmlFor="username" key="username-label">Username</InputLabel>
            <Input id="username" key="username-input" onChange={event => this.setState({ username: event.target.value })} />
          </FormControl>
          <FormControl className="form-control" key="form-control-2">
            <InputLabel htmlFor="password" key="password-label">Password</InputLabel>
            <Input id="password" type="password" key="password-input"
              onChange={event => this.setState({ password: event.target.value })}
            />
          </FormControl>
          <FormControl className="form-control" key="form-control-3">
            <InputLabel htmlFor="accessKey" key="accessKey-label">Access Key</InputLabel>
            <Input id="accessKey" key="accessKey-input"
              onChange={event => this.setState({ accessKey: event.target.value })}
              onKeyDown={event => this.handleKeyDown(event)}
            />
          </FormControl>
          <Button raised color="primary" className="button" key="register-button"
            onClick={this.register}
            disabled={buttonDisabled}
          >
            REGISTER
          </Button>
          {this.renderErrorMessage()}
          {this.renderProgress()}
        </CardContent>
      </Card>
    );
  }
}

RegisterCard.PropTypes = {
  classes: PropTypes.object.isRequired,
  closeButtonClicked: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    isRegistering: state.user.isRegistering,
    registrationError: state.user.registrationError
  };
};

const ConnectedRegisterCard = connect(
  mapStateToProps
)(RegisterCard);

export default ConnectedRegisterCard;
