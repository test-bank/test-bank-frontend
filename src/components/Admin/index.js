import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';

import Card from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';

import BackIcon from 'material-ui-icons/KeyboardBackspace';

import Admin from '../../modules/admin';

import '../../app/styles/components/admin.sass';

class AdminView extends React.Component {

  componentDidMount() {
    this.props.dispatch(Admin.actions.fetchAllUsers());
  }

  @autobind
  goBack() {
    this.props.dispatch(Admin.actions.toggleAdminPage());
  }

  renderAdminCard() {
    const users = this.props.users.map(user => {
      return (
        <span>{user.username}</span>
      );
    });

    return (
      <Card raised className="admin-users-card">
        {users}
      </Card>
    );
  }

  renderBackCard() {
    return (
      <Card raised className="admin-back-card">
        <IconButton onClick={this.goBack}>
          <BackIcon />
        </IconButton>
      </Card>
    );
  }

  render() {
    return (
      <div className="admin">
        {this.renderBackCard()}
        {this.renderAdminCard()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isFetchingUsers: state.admin.isFetchingUsers,
    users: state.admin.users,
    fetchUsersError: state.admin.fetchUsersError
  };
};

export default connect(
  mapStateToProps
)(AdminView);
