import React from 'react';
import autobind from 'autobind-decorator';

import { CircularProgress } from 'material-ui/Progress';
import Typography from 'material-ui/Typography';
import { ListItem, ListItemText, ListItemIcon } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';

import CheckCircle from 'material-ui-icons/CheckCircle';
import FileIcon from 'material-ui-icons/InsertDriveFile';
import Cancel from 'material-ui-icons/Cancel';
import Close from 'material-ui-icons/Close';

export default class FileListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
  }

  @autobind
  closeButtonClicked(event) {
    event.stopPropagation();
    event.preventDefault();
    this.props.onCloseClick();
  }

  renderIcon() {
    const status = this.props.file.status;
    if (status === 'Uploading') {
      return <CircularProgress size={25} mode="indeterminate" />;
    } else if (status === 'Done') {
      return <CheckCircle className="upload-dialog-checkmark" />;
    }
    return <Cancel className="upload-dialog-cancel" />;
  }

  renderMessageview() {
    const { status, message } = this.props.file;
    let className;
    if (status === 'Done') {
      className = 'success';
    } else {
      className = 'error';
    }
    const expanded = this.state.expanded ? 'upload-dialog-listitem-expanded' : '';
    return (
      <ListItem className={`upload-dialog-listitem-${className} ${expanded}`}>
        <Typography className="upload-dialog-listitem-text">{message}</Typography>
      </ListItem>
    );
  }

  render() {
    const file = this.props.file;
    return (
      <div>
        <ListItem divider={true} button={this.props.file.status !== 'Uploading'}
          onClick={() => this.setState({ expanded: !this.state.expanded })}
        >
          <ListItemIcon>
            <FileIcon />
          </ListItemIcon>
          <ListItemText primary={file.name} secondary={file.course} />
          {this.renderIcon()}
          <IconButton className="upload-dialog-close" onClick={this.closeButtonClicked}>
            <Close className="upload-dialog-close-icon"/>
          </IconButton>
        </ListItem>
        {this.renderMessageview()}
      </div>
    );
  }
}
