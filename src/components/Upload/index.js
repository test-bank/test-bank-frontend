import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';

import UploadModule from '../../modules/upload';

import Paper from 'material-ui/Paper';
import { CircularProgress } from 'material-ui/Progress';
import Typography from 'material-ui/Typography';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
import FileListItem from './FileListItem';
import IconButton from 'material-ui/IconButton';

import ExpandMore from 'material-ui-icons/ExpandMore';
import Close from 'material-ui-icons/Close';

import '../../app/styles/components/upload.sass';

class Upload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
  }

  @autobind
  close(event) {
    event.stopPropagation();
    this.props.dispatch(UploadModule.actions.clear());
  }

  handleFileRemoval(file) {
    if (file.status !== 'Uploading') {
      this.props.dispatch(UploadModule.actions.removeFile(file));
    } else {
      this.props.dispatch(UploadModule.actions.abortFileUpload(file));
    }
  }

  renderCloseButton() {
    if (this.props.uploadingFilesCount === 0 && !this.state.expanded) {
      return (
        <IconButton className="upload-dialog-close" onClick={this.close}>
          <Close className="upload-dialog-close-icon"/>
        </IconButton>
      );
    }
    return null;
  }

  renderFinished() {
    const { uploadingFilesCount, completedFilesCount } = this.props;
    if (uploadingFilesCount === 0 && !this.state.expanded && completedFilesCount > 0) {
      return (
        <div className="upload-dialog-finished">
          <Typography className="upload-dialog-finished-text">{completedFilesCount}</Typography>
        </div>
      );
    }
    return null;
  }

  renderFailed() {
    const { uploadingFilesCount, failedFilesCount } = this.props;
    if (uploadingFilesCount === 0 && !this.state.expanded && failedFilesCount > 0) {
      return (
        <div className="upload-dialog-failed">
          <Typography className="upload-dialog-failed-text">{failedFilesCount}</Typography>
        </div>
      );
    }
    return null;
  }

  renderHeader() {
    const { uploadingFilesCount } = this.props;
    const titleClass = this.state.expanded ? 'upload-dialog-title-expanded' : '';
    return (
      <div className="upload-dialog-header"
        onClick={() => this.setState({ expanded: !this.state.expanded })}
      >
        <Typography className={`upload-dialog-title ${titleClass}`}>Uploads</Typography>
        <div className="upload-dialog-header-right">
          {this.renderFailed()}
          {uploadingFilesCount > 0 && !this.state.expanded ? <CircularProgress size={25} mode="indeterminate" /> : null}
          {this.renderFinished()}
          {this.renderCloseButton()}
        </div>
        {this.state.expanded ? <ExpandMore className="upload-dialog-chevron"/> : null}
      </div>
    );
  }

  renderBody() {
    return (
      <List className="upload-dialog-list" dense>
        {this.props.files.map((file, index) => (
          <FileListItem file={file} key={`${file.name}-${index}`}
            onCloseClick={() => this.handleFileRemoval(file)}
          />
        ))}
      </List>
    );
  }

  render() {
    if (this.props.files.length === 0) return null;
    const dialogClassName = this.state.expanded ? 'upload-dialog-expanded' : 'upload-dialog';
    return (
      <Paper className={dialogClassName} elevation={10}>
        {this.renderHeader()}
        <Divider />
        <div className="upload-dialog-body">
          {this.state.expanded ? this.renderBody() : null}
        </div>
      </Paper>
    );
  }
}

const mapStateToProps = state => {
  return {
    files: state.upload.files,
    completedFilesCount: UploadModule.selectors.completedFilesCount(state),
    failedFilesCount: UploadModule.selectors.failedFilesCount(state),
    uploadingFilesCount: UploadModule.selectors.uploadingFilesCount(state)
  };
};

export default connect(
  mapStateToProps
)(Upload);
