import actions from './action-types';
import { insertItem, removeItem } from '../../lib/immutable-helpers';

const initialState = {
  isFetching: false,
  courses: [],
  error: null,
  deleting: []
};

function testbank(state = initialState, action) {
  switch (action.type) {
    case actions.TESTBANK_FETCH_BEGIN:
      return Object.assign({}, state, {
        isFetching: true
      });
    case actions.TESTBANK_FETCH_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        courses: action.testbank
      });
    case actions.TESTBANK_FETCH_ERROR:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case actions.MERGE_FILE: {
      const updatedCourses = updateCourseDirectory(state.courses, action.course, action.file, action.category);
      return Object.assign({}, state, {
        courses: updatedCourses
      });
    }
    case actions.TESTBANK_DELETE_BEGIN: {
      const array = insertItem(state.deleting, action.key);
      return Object.assign({}, state, {
        deleting: array
      });
    }
    case actions.TESTBANK_DELETE_SUCCESS: {
      const array = removeItem(state.deleting, action.key);
      const file = action.key.split('/')[1];
      const course = action.key.split('/')[0];
      const updatedCourses = removeFileFromCourse(state.courses, file, course);
      return Object.assign({}, state, {
        deleting: array,
        courses: updatedCourses
      });
    }
    case actions.TESTBANK_DELETE_FAILED: {
      const array = removeItem(state.deleting, action.key);
      return Object.assign({}, state, {
        deleting: array
      });
    }
    default:
      return state;
  }
}

function removeFileFromCourse(courses, file, course) {
  const index = courses.findIndex(c => c.name === course);
  const toUpdate = courses[index];
  const directory = Object.keys(toUpdate.directory).find(dir => {
    const files = toUpdate.directory[dir];
    return files.some(f => f.name === file);
  });
  const updatedDirectory = toUpdate.directory[directory].filter(f => f.name !== file);
  toUpdate.directory[directory] = updatedDirectory;
  if (toUpdate.directory[directory].length === 0) {
    delete toUpdate.directory[directory];
  }
  courses[index] = toUpdate;
  return courses;
}

function updateCourseDirectory(courses, course, file, category) {
  const index = courses.findIndex(c => c.name === course);
  const toUpdate = courses[index];
  const innerDirectory = toUpdate.directory[category];
  if (innerDirectory) {
    const updatedInnerDirectory = innerDirectory.concat(file);
    toUpdate.directory[category] = updatedInnerDirectory;
  } else {
    toUpdate.directory[category] = [file];
  }
  courses[index] = toUpdate;
  return courses;
}

export default testbank;
