import actions from './action-types';
import fetch from 'isomorphic-fetch';
import config from 'config';

function beginTestbankFetch() {
  return {
    type: actions.TESTBANK_FETCH_BEGIN
  };
}

function testbankFetchSuccess(testbank) {
  return {
    type: actions.TESTBANK_FETCH_SUCCESS,
    testbank
  };
}

function testbankFetchError(error) {
  return {
    type: actions.TESTBANK_FETCH_ERROR,
    error
  };
}

function beginFileDeletion(key) {
  return {
    type: actions.TESTBANK_DELETE_BEGIN,
    key
  };
}

function fileDeletionSuccess(key) {
  return {
    type: actions.TESTBANK_DELETE_SUCCESS,
    key
  };
}

function fileDeletionFailed(key) {
  return {
    type: actions.TESTBANK_DELETE_FAILED,
    key
  };
}

function fetchTestbank() {
  return dispatch => {
    dispatch(beginTestbankFetch());
    const params = {
      method: 'GET',
      credentials: 'include'
    };
    return fetch(`${config.api.baseUrl}/api/testbank`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status === 200) {
          dispatch(testbankFetchSuccess(json.testbank));
        } else {
          dispatch(testbankFetchError(json.error));
        }
      });
  };
}

function deleteFile(file, course) {
  return dispatch => {
    const key = `${course}/${file}`;
    dispatch(beginFileDeletion(key));
    const params = {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify({ key }),
      headers: { 'Content-Type': 'application/json' }
    };
    return fetch(`${config.api.baseUrl}/api/testbank`, params)
    .then(response => response.json())
    .then(json => {
      if (json.status === 200) {
        dispatch(fileDeletionSuccess(key));
      } else {
        dispatch(fileDeletionFailed(key));
      }
    });
  };
}

export default {
  fetchTestbank,
  deleteFile
};
