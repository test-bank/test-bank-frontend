import { createSelector } from 'reselect';

const testbank = state => state.testbank.courses;

export const getTestbank = createSelector(
  [testbank],
  testbank => Object.keys(testbank).map((key) => {
    const name = key;
    const directory = testbank[key];
    return { name, directory };
  })
);
