import actions from './actions';
import selectors from './selectors';

export default {
  actions,
  selectors
};
