import actions from './action-types';

const initialState = {
  currentUser: null,
  loginError: null,
  isLoggingIn: false,
  isRegistering: false,
  registrationError: null,
  isLoggingOut: false
};

function user(state = initialState, action) {
  switch (action.type) {
    case actions.LOGIN_INITIATED:
      return Object.assign({}, state, {
        isLoggingIn: true
      });
    case actions.LOGIN_SUCCESSFUL:
      return Object.assign({}, state, {
        isLoggingIn: false,
        currentUser: action.user,
        loginError: null
      });
    case actions.LOGIN_FAILED:
      return Object.assign({}, state, {
        isLoggingIn: false,
        loginError: action.error
      });
    case actions.REGISTRATION_INITIATED:
      return Object.assign({}, state, {
        isRegistering: true
      });
    case actions.REGISTRATION_SUCCESSFUL:
      return Object.assign({}, state, {
        isRegistering: false,
        currentUser: action.user,
        registrationError: null
      });
    case actions.REGISTRATION_FAILED:
      return Object.assign({}, state, {
        isRegistering: false,
        registrationError: action.error
      });
    case actions.ERROR_RESET:
      return Object.assign({}, state, {
        loginError: null,
        registrationError: null
      });
    case actions.LOGOUT_INITIATED:
      return Object.assign({}, state, {
        isLoggingOut: true
      });
    case actions.LOGOUT_SUCCESSFUL:
      return Object.assign({}, state, {
        isLoggingOut: false,
        currentUser: null
      });
    default:
      return state;
  }
}

export default user;
