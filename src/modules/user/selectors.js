import { createSelector } from 'reselect';

const getCurrentUser = state => state.currentUser;

const isAdmin = createSelector(
  [getCurrentUser],
  currentUser => currentUser && currentUser.role === 'admin'
);

export default {
  isAdmin
};
