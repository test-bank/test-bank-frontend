import actions from './action-types';
import fetch from 'isomorphic-fetch';
import config from 'config';

function loginInitiated() {
  return {
    type: actions.LOGIN_INITIATED
  };
}

function loginSuccessful(user) {
  return {
    type: actions.LOGIN_SUCCESSFUL,
    user
  };
}

function loginFailed(error) {
  return {
    type: actions.LOGIN_FAILED,
    error
  };
}

function registrationInitated() {
  return {
    type: actions.REGISTRATION_INITIATED
  };
}

function registrationSuccessful(user) {
  return {
    type: actions.REGISTRATION_SUCCESSFUL,
    user
  };
}

function registrationFailed(error) {
  return {
    type: actions.REGISTRATION_FAILED,
    error
  };
}

function clearErrors() {
  return {
    type: actions.ERROR_RESET
  };
}

function logoutInitiated() {
  return {
    type: actions.LOGOUT_INITIATED
  };
}

function logoutSuccessful() {
  return {
    type: actions.LOGOUT_SUCCESSFUL
  };
}

const loginErrorMappings = {
  404: 'No user with that username exists! How fucking stupid are you? Please make sure it is correct and try again.',
  401: 'Invalid credentials. Dumbass, double check your password and try again.',
  500: 'Uh oh! Something is fucked up on my end. W/e go fuck yourself.'
};

const registrationErrorMappings = {
  'Invalid registration key supplied.': 'Invalid access key! Use the fucking key given to you. You are a complete moron.',
  'Username taken': 'Username taken! Sucks to suck you fucking pussy. Someone beat you to it.'
};

function login(username, password) {
  return dispatch => {
    dispatch(loginInitiated());
    const params = {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include'
    };
    return fetch(`${config.api.baseUrl}/login`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status === 200) {
          dispatch(loginSuccessful(json.user));
        } else {
          dispatch(loginFailed(loginErrorMappings[json.status]));
        }
      });
  };
}

function fetchCurrentUser() {
  return dispatch => {
    const params = {
      method: 'GET',
      credentials: 'include'
    };
    return fetch(`${config.api.baseUrl}/me`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status === 200) {
          dispatch(loginSuccessful(json.user));
        }
      });
  };
}

function register(username, password, secretKey) {
  return dispatch => {
    dispatch(registrationInitated());
    const params = {
      method: 'POST',
      body: JSON.stringify({ username, password, secretKey, role: 'user' }),
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include'
    };
    return fetch(`${config.api.baseUrl}/register`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status === 200) {
          dispatch(registrationSuccessful(json.user));
        } else if (json.status >= 500) {
          dispatch(registrationFailed(loginErrorMappings[500]));
        } else {
          dispatch(registrationFailed(registrationErrorMappings[json.message]));
        }
      });
  };
}

function logout() {
  return dispatch => {
    dispatch(logoutInitiated());
    const params = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include'
    };
    return fetch(`${config.api.baseUrl}/logout`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status === 200) {
          dispatch(logoutSuccessful());
        }
      });
  };
}

export default {
  login,
  fetchCurrentUser,
  register,
  clearErrors,
  logout
};
