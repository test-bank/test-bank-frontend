import { createStore, applyMiddleware, compose } from 'redux';
import { combineReducers } from 'redux';
import user from './user/reducers';
import testbank from './testbank/reducers';
import admin from './admin/reducers';
import upload from './upload/reducers';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

const reducers = combineReducers({
  user,
  testbank,
  admin,
  upload
});

const loggerMiddleware = createLogger();

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
  : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunkMiddleware, loggerMiddleware)
);

const flux = createStore(
  reducers,
  enhancer
);

export default flux;
