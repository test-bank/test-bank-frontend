import { createSelector } from 'reselect';

const uploads = state => state.upload.files;

const uploadingFilesCount = createSelector(
  uploads,
  uploads => uploads.filter(file => file.status === 'Uploading').length
);

const completedFilesCount = createSelector(
  uploads,
  uploads => uploads.filter(file => file.status === 'Done').length
);

const failedFilesCount = createSelector(
  uploads,
  uploads => uploads.filter(file => file.status === 'Failed').length
);

export default {
  uploadingFilesCount,
  completedFilesCount,
  failedFilesCount
};
