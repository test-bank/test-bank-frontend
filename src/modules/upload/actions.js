import actions from './action-types';
import TestbankActions from '../testbank/action-types';
import fetch from 'isomorphic-fetch';
import config from 'config';

function fileUploadStarted(file) {
  return {
    type: actions.FILE_UPLOAD_START,
    file
  };
}

function fileUploadChanged(file) {
  return {
    type: actions.FILE_UPLOAD_CHANGED,
    file
  };
}

function generatePayload(course, file, status, message) {
  return { course, name: file.name, status, message };
}

function uploadFile(course, file) {
  return dispatch => {
    dispatch(fileUploadStarted(generatePayload(course, file, 'Uploading')));

    const formData = new FormData();
    formData.append('course', course);
    formData.append('file', file);

    const params = {
      method: 'POST',
      credentials: 'include',
      body: formData
    };

    return fetch(`${config.api.baseUrl}/api/testbank/upload`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status === 200) {
          const { file, category } = json.data;
          dispatch(fileUploadChanged(generatePayload(course, file, 'Done', json.message)));
          dispatch({ type: TestbankActions.MERGE_FILE, course, file, category });
        } else {
          dispatch(fileUploadChanged(generatePayload(course, file, 'Failed', json.message)));
        }
      })
      .catch(error => {
        dispatch(fileUploadChanged(generatePayload(course, error.file, 'Failed', error.message)));
      });
  };
}

function uploadFailed(course, file) {
  return dispatch => {
    dispatch(fileUploadStarted(generatePayload(course, file, 'Failed', `This file already exists in ${course}, you stupid fuck.`)));
  };
}

function clear() {
  return dispatch => {
    dispatch({ type: actions.CLEAR_UPLOADS });
  };
}

function removeFile(file) {
  return dispatch => {
    dispatch({ type: actions.REMOVE_UPLOAD, file });
  };
}

function abortFileUpload(file) {
  return dispatch => {
    const params = {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify({ file: file.name, course: file.course }),
      headers: { 'Content-Type': 'application/json' }
    };

    return fetch(`${config.api.baseUrl}/api/testbank/upload/abort`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status !== 200) {
          dispatch(fileUploadChanged(generatePayload(file.course, file, 'Done', json.message)));
        }
      });
  };
}

export default {
  uploadFile,
  uploadFailed,
  clear,
  removeFile,
  abortFileUpload
};
