import actions from './action-types';
import { insertItem, removeItem, replaceItemAtIndex } from '../../lib/immutable-helpers';

const initialState = {
  files: []
};

function upload(state = initialState, action) {
  switch (action.type) {
    case actions.FILE_UPLOAD_START: {
      return Object.assign({}, state, {
        files: insertItem(state.files, action.file)
      });
    }
    case actions.FILE_UPLOAD_CHANGED: {
      const index = state.files.findIndex(file => file.name === action.file.name && file.course === action.file.course);
      return Object.assign({}, state, {
        files: replaceItemAtIndex(state.files, action.file, index)
      });
    }
    case actions.CLEAR_UPLOADS:
      return Object.assign({}, state, {
        files: []
      });
    case actions.REMOVE_UPLOAD:
      return Object.assign({}, state, {
        files: removeItem(state.files, action.file)
      });
    default:
      return state;
  }
}

export default upload;
