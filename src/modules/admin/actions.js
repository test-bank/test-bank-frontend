import actions from './action-types';
import fetch from 'isomorphic-fetch';
import config from 'config';

function toggleAdminPage() {
  return {
    type: actions.TOGGLE_ADMIN_PAGE
  };
}

function fetchUsersInitiated() {
  return {
    type: actions.FETCH_USERS_INITIATED
  };
}

function fetchUsersSuccess(users) {
  return {
    type: actions.FETCH_USERS_SUCCESS,
    users
  };
}

function fetchUsersFailed(error) {
  return {
    type: actions.FETCH_USERS_FAILED,
    error
  };
}

const fetchAllUsersMap = {
  500: `It's not you, it's me 😭. Please refresh and try again, or don't, idgaf.`,
  401: `How you got here, I have no idea. But you shouldn't be here, so get the fuck out.`
};

function fetchAllUsers() {
  return dispatch => {
    dispatch(fetchUsersInitiated());
    const params = {
      credentials: 'include'
    };
    return fetch(`${config.api.baseUrl}/api/admin/users`, params)
      .then(response => response.json())
      .then(json => {
        if (json.status === 200) {
          dispatch(fetchUsersSuccess(json.users));
        } else {
          dispatch(fetchUsersFailed(fetchAllUsersMap[json.status]));
        }
      });
  };
}

export default {
  toggleAdminPage,
  fetchAllUsers
};
