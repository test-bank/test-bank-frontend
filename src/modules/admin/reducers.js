import actions from './action-types';

const initialState = {
  isAdminPageOpen: false,
  isFetchingUsers: false,
  users: [],
  fetchUsersError: {}
};

function admin(state = initialState, action) {
  switch (action.type) {
    case actions.TOGGLE_ADMIN_PAGE:
      return Object.assign({}, state, {
        isAdminPageOpen: !state.isAdminPageOpen
      });
    case actions.FETCH_USERS_INITIATED:
      return Object.assign({}, state, {
        isFetchingUsers: true
      });
    case actions.FETCH_USERS_SUCCESS:
      return Object.assign({}, state, {
        isFetchingUsers: false,
        users: action.users
      });
    case actions.FETCH_USERS_FAILED:
      return Object.assign({}, state, {
        isFetchingUsers: false,
        fetchUsersError: action.error
      });
    default:
      return state;
  }
}

export default admin;
