import React from 'react';
import ReactDOM from 'react-dom';
import Root from '../components/root';
import 'babel-polyfill';
import 'typeface-roboto';
import './styles/_base.sass';
import '../modules/flux';

export default function run() {
  const renderTo = document.createElement('div');
  document.body.appendChild(renderTo);
  ReactDOM.render(<Root />, renderTo);
}

run();
