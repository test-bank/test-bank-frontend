var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var FaviconsWebpackPlugin = require('favicons-webpack-plugin')
var env = process.env.NODE_ENV || 'dev';

module.exports = {
  entry: './src/app/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'yaGsIsemaJ'
    }),
    new FaviconsWebpackPlugin(__dirname + '/favicon.png')
  ],
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.css$/, loaders: 'style-loader!css-loader'},
      { test: /\.sass$/, loader: 'style-loader!css-loader!sass-loader?indentedSyntax'},
      { test: /\.scss$/, loader: 'style-loader!cs-loader!sass-loader?'},
      { test: /\.(eot|svg|ttf|woff|woff2)$/, loader: 'file-loader'},
      { test: /\.(png|jpg)$/, loader: 'url-loader?limit=25000' },
    ]
  },
  resolve: {
    alias: {
      config$: __dirname + '/config/' + env + '.js'
    }
  }
};
