module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true,
        },
        "sourceType": "module",
        "allowImportExportEverywhere": false,
        "codeFrame": false
    },
    "plugins": [
        "react"
    ],
    "rules": {
      "react/jsx-uses-react": "error",
      "react/jsx-uses-vars": "error",
      "jsx-quotes": [2, "prefer-double"],
      "comma-dangle": [2,"never"],
      "no-cond-assign": [2,"except-parens"],
      "no-console": 2,
      "no-constant-condition": 2,
      "no-dupe-args": 2,
      "no-dupe-keys": 2,
      "no-empty": 2,
      "no-extra-boolean-cast": 2,
      "no-extra-semi": 2,
      "no-irregular-whitespace": 2,
      "no-negated-in-lhs": 2,
      "no-unreachable": 2,
      "no-unused-vars": 1,
      "default-case": 2,
      "dot-notation": 2,
      "eqeqeq": [2, "smart"],
      "no-else-return": 2,
      "no-extra-bind": 2,
      "no-floating-decimal": 2,
      "no-self-compare": 2,
      "yoda": [2, "never",{}],
      "array-bracket-spacing": [2,"never",{}],
      "brace-style": [2,"1tbs",{"allowSingleLine":true}],
      "camelcase": [2,{"properties":"always"}],
      "comma-spacing": [2,{"after":true}],
      "eol-last": 2,
      "indent": [2, 2, {"SwitchCase": 1}],
      "key-spacing": [2,{"afterColon":true}],
      "no-lonely-if": 2,
      "no-mixed-spaces-and-tabs": [2,"smart-tabs"],
      "no-trailing-spaces": [2,{"skipBlankLines":true}],
      "no-unneeded-ternary": 2,
      "object-curly-spacing": [2,"always",{"objectsInObjects":true}],
      "quotes": [2,"single",{"avoidEscape": true, "allowTemplateLiterals": true}],
      "semi": [2,"always"],
      "space-before-blocks": [2,"always"],
      "arrow-spacing": [2,{"before":true,"after":true}],
      "constructor-super": 2,
      "no-const-assign": 2,
      "no-this-before-super": 2,
      "no-var": 2,
      "object-shorthand": [2,"always"],
      "prefer-const": 2,
      "prefer-spread": 2
    }
};
